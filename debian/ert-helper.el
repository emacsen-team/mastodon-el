(require 'mastodon)
(require 'mastodon-notifications)

(ert-run-tests-batch-and-exit
 '(not (or
        ;; require network access
        "mastodon-auth--handle-token-response--good"
        ;; (void-function image-transforms-p)
        "mastodon-media--get-avatar-rendering"
        "mastodon-media--inline-images"
        "mastodon-media--load-image-from-url-avatar-with-imagemagic"
        "mastodon-media--load-image-from-url-avatar-without-imagemagic"
        "mastodon-media--load-image-from-url-media-link-with-imagemagic"
        "mastodon-media--load-image-from-url-media-link-without-imagemagic"
        "mastodon-media--load-image-from-url-url-fetching-fails"
        "mastodon-media--process-image-response"
        ;; (error "Invalid image type ‘png’")
        "mastodon-profile--add-author-bylines"
        "mastodon-profile--make-author-buffer"
        ;; flaky tests depending on time handling
        "mastodon-tl--byline-timestamp-has-relative-display"
        "mastodon-tl--next-tab-item--no-spaces-at-ends"
        "mastodon-tl--next-tab-item--with-spaces-at-ends"
        )))
